---
layout: post
index: 5
title: Casio Notebooks
platforms: colorlcd
---

## Picky Talk

* Model: JD-363

<div class="centered">
<img src="{{site.baseurl}}/res/pickytlk/IMG_2364.JPG" style="display: inline-block; width: 45%" alt=""/>
</div>

* Model: JD-364
* PCB Revision: A140293A-1 Z814-1
* ROM: NEC D23C8000LWGX-C12 9536K7016

<div class="centered">
<img src="{{site.baseurl}}/res/pickytlk/IMG_2632.JPG" style="display: inline-block; width: 45%" alt=""/>
<img src="{{site.baseurl}}/res/pickytlk/IMG_2636.JPG" style="display: inline-block; width: 45%" alt=""/>
</div>

* Model: JD-368
* PCB Revision: A140723-1 Z834-1
* ROM: NEC D23C8000XGX-C42 9737K7041

<div class="centered">
<img src="{{site.baseurl}}/res/pickytlk/IMG_2614.JPG" style="display: inline-block; width: 45%" alt=""/>
<img src="{{site.baseurl}}/res/pickytlk/IMG_2626.JPG" style="display: inline-block; width: 45%" alt=""/>
</div>

* Model: JD-370
* PCB Revision: A140947-1 Z835-1

<div class="centered">
<img src="{{site.baseurl}}/res/pickytlk/IMG_1519.JPG" style="display: inline-block; width: 45%" alt=""/>
<img src="{{site.baseurl}}/res/pickytlk/IMG_1524.JPG" style="display: inline-block; width: 45%" alt=""/>
</div>

To dump CPU's internal ROM, we can just readout `0x8000` bytes from segment `0`. I didn't bother with rendering hex on the screen or sending via serial port, just wrote to some address in RAM, so that the data bus could be sniffed with a logic analyzer (for good measure I [wrote 8 times each byte](https://github.com/qufb/data/blob/main/cfx9850gb/scripts/cpu_rom_dumper.ram_0x1000.x8.py) and [parsed the max result of each byte](https://github.com/qufb/data/blob/main/cfx9850gb/scripts/parse8.py) to build the final ROM).

Additionally to the relevant ROM lines, I also soldered 2 enameled wires directly to RAM's <span class="label" style="background: darkred">CE1</span> and <span class="label" style="background: darkblue">WE</span> pins. Below you can see the first 2 sniffed bytes: <span class="label" style="background: #d476c4">ff 88</span> (from a previous test where I wrote each byte only once).

<div class="centered">
<img src="{{site.baseurl}}/res/pickytlk/IMG_1832.JPG" style="display: inline-block; max-height: 15rem" alt=""/>
<img src="{{site.baseurl}}/res/pickytlk/cpu_trace.png" style="display: inline-block; max-height: 15rem" alt=""/>
</div>

For instruction tests, you want to grab X1 clock signal. Not sure what's the usual way to sniff these, but you can see that two pins are connected by resistor R28 (let's call them pins 1 and 3). You can grab pin 4 (opposite to pin 1), since pin 5 is GND, and pin 6 will shut the whole system down...

## Plet's

* Model: MK-300; MK-350
* PCB Revision: A141252-1 Z836-1
* ROM: NEC D23C8000XGX-C77 9936K7017; OKI M538032E-48 0345404

<div class="centered">
<img src="{{site.baseurl}}/res/pickytlk/IMG_2602.JPG" style="display: inline-block; width: 45%" alt=""/>
<img src="{{site.baseurl}}/res/pickytlk/IMG_2610.JPG" style="display: inline-block; width: 45%" alt=""/>
</div>

