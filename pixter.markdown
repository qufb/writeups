---
layout: post
index: 6
title: Mattel Pixter
platforms: pixter
---

See also previous research on [eLinux](https://elinux.org/Pixter_Multimedia).

## Tooling

* [Ghidra loader](https://github.com/qufb/PixterMultimediaLoader)

## JTAG

* OpenOCD configs: [adapter](https://gitlab.com/qufb/dump/-/blob/master/pixtermu/openocd/ftdi.cfg), [target](https://gitlab.com/qufb/dump/-/blob/master/pixtermu/openocd/lh79524.cfg)

For the adapter, I used a CJMCU-232H, which is based on FT232H. These clones are cheap but well supported in OpenOCD. Other adapters might work as well, such as [Raspberry Pi](https://github.com/openocd-org/openocd/blob/573a39b36cf133bb7403b12337301a5616112f1a/tcl/interface/sysfsgpio-raspberrypi.cfg).

Connect the adapter to your PC, then power on the console. As documented in the [datasheet](https://www.nxp.com/docs/en/data-sheet/LH79524_525_N.pdf), if TEST1 pin is pulled-down, and other JTAG pins are connected, embedded ICE mode is enabled. The console should be halted, waiting for the debugger to connect. To do this, run `openocd -f ftdi.cfg -f lh79524.cfg`. You should get the message "accepting 'telnet' connection on tcp/4444".

LH79524 memory maps can be directly read (verify in telnet by running `mdw 0x44000000 10`), but as commented in the configs, you might need to halt at different stages depending on what's being dumped:

* **Boot ROM**: According to "[LH79524 User's Guide](https://www.nxp.com/docs/en/user-guide/LH79524-LH79525_UG_V1_3.pdf) - 3.1 Theory of Operation", Boot ROM is unmapped from `0x80000000` after it finishes loading other ROMs. In OpenOCD, attempting to read it afterwards results in data aborts. This gets complicated with LH79524, since the first device presented in the JTAG daisy-chain is a boundary scan TAP, not the CPU core TAP (the one we can read memory maps).
    * I haven't figured out how to configure OpenOCD to support multiple devices, and transfer control to the next one cleanly. A workaround is implemented in the script, which after connecting, will run `init` and `delay_halt` (custom command that pulses nTRST, but might leave the system in an unreliable state). If this doesn't work, you can alternatively comment those two commands, and instead run `soft_reset_halt`.
    * Afterwards, run `dump_image 0x80000000.bin 0x80000000 0x2000`.
* **Other ROMs**: The procedure above should also work. Alternatively, you can run `reset`, wait for cart program to be executed, then `halt`.
    * For CS1 ROM: `dump_image 0x44000000.bin 0x44000000 0x400000`.
    * For CS2 ROM: `dump_image 0x48000000.bin 0x48000000 0x400000`. Note that some carts might just be 2MB, so the second half will be mirrored. Unknown at this point if any use CS3, which can be seen in the [expansion slot pinout](https://elinux.org/Pixter_Multimedia_Expansion_Slot) (also on [Pixter Color](https://elinux.org/Pixter_Expansion_Slot)).

Here are some sanity checks for validating cart ROM dumps:

* Starts with bytes `\xcc\x66\x55\xaa` (these are checked by CS1 program @ `0x4402b764`;
* At offset `0x8` is an address to a pointer table, where its first entry usually points to an address right after the end of the table;
* At offsets `0x20, 0x24, 0x28` are addresses to thumb mode subroutines;
* Several instances of string "SUNPLUS SPEECH";

## PCB

* Model: H4651/J4287/J4288
* PCB Revision: PT1543A-BGA-4F2C 2005/04/30 Rev 4.2b

<div class="centered">
<img src="{{site.baseurl}}/res/pixtermu/PCB_Front.JPG" style="display: inline-block; width: 45%" alt=""/>
<img src="{{site.baseurl}}/res/pixtermu/PCB_Back.JPG" style="display: inline-block; width: 45%" alt=""/>
</div>
