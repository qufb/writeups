---
layout: post
index: 7
title: Hasbro Giga Pets Explorer
platforms: gigapets
---

* PCB Markings: 3059L-R3 0626 / 3059TV-R3B / 3059R-R1 0626

<div class="centered">
<img src="{{site.baseurl}}/res/gigapets/IMG_2454.JPG" style="display: inline-block; width: 45%" alt=""/>
<img src="{{site.baseurl}}/res/gigapets/IMG_2457.JPG" style="display: inline-block; width: 45%" alt=""/>
</div>

By following the traces from blob U7, we can see that it follows the 48-pin TSOP1 Standard Type pinout (there are several candidate flash ROMs, e.g. K8D6x16UTM):

* Pins 11, 12, 14, 15 (`~WE, ~RESET, ~WP/ACC, RY/~BY`) are not connected: these are all optional for just reading ROM contents (no need to write flash contents, reset, modify hardware protections, or even wait for ready output);
* Pin 13 (`A21`) is connected: suggests that ROM size is 64Mbit;
* Pin 26, 28 (`~OE, ~CE`) are shorted: usually these are set low one at a time, but can also be set both at the same time;
* Pin 27, 46 (`Vss`) are shorted and read `0V`;
* Pin 37 (`Vcc`) reads `3.28V`;
* Pin 47 (`~BYTE`) reads `3.28V`: suggests that ROM data is always read with word selection;
* Other pins with varying signals (e.g. reading `1.7V`) suggests being address/data inputs/outputs; 

As usual with chip-on-board ROMs, traces to the CPU were cut to avoid contending accesses, and the system was powered by itself instead of by the programmer.

Soldering to such small TSOP pads required using 0.2mm enamelled wire with a 0.5mm tip. As an aside, my JBC has some funky tips that they would like to convince you to only replace with a ["removal device Ref. 0114108"](https://www.jbctools.com/pdf/manual_DST_classic.pdf), but of course with more common tools can also be done. 🙂

<div class="centered">
<img src="{{site.baseurl}}/res/gigapets/IMG_2460.JPG" style="display: inline-block; max-height: 15rem" alt=""/>
</div>

Besides the required pins for reading ROM contents, the programmer also needs to have pins `~WE, ~RESET, ~WP/ACC, RY/~BY` pulled-high (e.g. with 4.7k resistors). I tried to keep wires around 10cm long, so things were a bit snuggled together:

<div class="centered">
<img src="{{site.baseurl}}/res/gigapets/IMG_2472.JPG" style="display: inline-block; max-height: 30rem" alt=""/>
</div>

