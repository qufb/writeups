---
layout: post
title: Beena there, done that
date: 2023-06-26 18:00:00 +0000
blurb: Doing JTAG without a JTAG port
platforms: ochaken
---

Busy with Beena emulation, but decided to dump something else... kinda? A [TV game system called 「テレビとお茶札 お茶犬『ほっ』と生活」](https://game.watch.impress.co.jp/docs/20050811/toy191.htm), which I won't even attempt translating, so it's "TV Ocha-Ken":

<div class="centered" style="width: 16rem">
<img src="{{site.baseurl}}/res/ochaken/ochaken.jpg" alt=""/>
</div>

How about I take off that cover:

<div class="centered">
<img src="{{site.baseurl}}/res/ochaken/pcb_ochaken.jpg" style="display: inline-block; width: 45%" alt=""/>
<img src="{{site.baseurl}}/res/ochaken/pcb_ochaken2.jpg" style="display: inline-block; width: 45%" alt=""/>
</div>

<div class="centered">
<img src="{{site.baseurl}}/res/ochaken/pcb_ochaken3.jpg" style="display: inline-block; width: 45%" alt=""/>
<img src="{{site.baseurl}}/res/ochaken/pcb_ochaken4.jpg" style="display: inline-block; width: 45%" alt=""/>
</div>

Better! [Sega Toys SoC strikes again]({{site.baseurl}}/2023/03/16/Climbing-the-Beenastalk#spelunking-time), but this time there's no JTAG port. Still, same pins, so we should be able to trace them and wire everything up to our dumper.

Two PCBs are attached together via jumper pins. This is the part where your heart skips a beat due to finding gunk around capacitors... although it doesn't look like leaked electrolytic fluid. Note how some is also near battery joints.

For composing PCB images, I used the same technique [Eric Schlaepfer illustrated on Hackaday Remoticon 2020](https://www.youtube.com/watch?v=BsftxTbs7MA). Instead of GIMP, I gave a try with Krita, since cage transform... actually works instead of glitching up?

Let's start with Beena. After some multimeter continuity tests, we get these traces for nTRST, with a bit of VCC + GND:

<div class="centered">
<img src="{{site.baseurl}}/res/ochaken/traces_beena.png" alt=""/>
</div>

<div class="centered">
<img src="{{site.baseurl}}/res/ochaken/traces_beena_back.png" alt=""/>
</div>

Caption:

* <span class="label" style="background: orange">ORANGE</span>: nTRST
* <span class="label" style="background: red">RED</span>: VCC
* <span class="label" style="background: brown">BROWN</span>: GND

Pinout:

|JTAG signal|JTAG pin|SoC pin|
|:--|--:|--:|
|nSRST|15|47|
|TDO|13|54|
|TCLK|9|53|
|TMS|7|52|
|TDI|5|51|
|nTRST|3|50|

This layout somewhat follows the [ARM recommendation](https://developer.arm.com/documentation/101416/0100/Hardware-Description/Target-Interfaces/ARM-Standard-JTAG) of keeping most logic signals connected to [pull-up resistors](https://www.youtube.com/watch?v=5vnW4U5Vj0k). Seems to be standard practice when we want to [eventually pull signals in the opposite direction](https://electronics.stackexchange.com/questions/437946/is-it-a-bad-idea-to-replace-pull-up-resistors-with-hard-pull-ups).

If we look around, we always find deviations in this layout. Beena JTAG is closer to:

<div class="centered">
<img src="{{site.baseurl}}/res/ochaken/jtag.png" alt=""/>
</div>

But there's also:

<div class="centered">
<img src="{{site.baseurl}}/res/ochaken/jtag2.png" alt=""/>
</div>

No pull-up on TDI and TDO, and didn't the recommendation talk about pulling-down TCK? Who is right here? My understanding is that, in general, pull-ups avoid flaky signals, but are not strictly necessary. [About TCK](https://e2e.ti.com/support/microcontrollers/arm-based-microcontrollers-group/arm-based-microcontrollers/f/arm-based-microcontrollers-forum/348680/jtag-pull-up-down-resistors-mandatory-or-optional---cortex_m4_0-jtag-communication-error-error--1170-0x0):

> [...] per ARM guidelines have a Pull Down so that at power up it does not create a fake TCK pulse. But a Pull Up would also be fine since in case of TM4C123 the JTAG is enabled after the internal POR reset is released.

Ok, I'll bite. As for SMD resistors, they are [valued 10k](https://kiloohm.info/smd3-resistor/103) like in examples (just don't get confused about labels `103` vs. `E01`).

Back to TV Ocha-Ken, SoC orientation is given by a small dot at the lower-left corner, from which pin 1 starts. If we follow pin 47 (nSRST), it's traced to a electronically actuating button, while pins 50..54 all go to 10k resistors, which in turn are all connected to a jumper pin:

<div class="centered">
<img src="{{site.baseurl}}/res/ochaken/traces_ochaken.png" alt=""/>
</div>

<div class="centered">
<img src="{{site.baseurl}}/res/ochaken/traces_ochaken_back.png" alt=""/>
</div>

On the other PCB, we can trace VCC from both DC jack and battery (<span class="label" style="background: orange">ORANGE</span> was used for the jumper pin trace, since it's indirectly connected to VCC via U6):

<div class="centered">
<img src="{{site.baseurl}}/res/ochaken/traces_ochaken2.png" alt=""/>
</div>

<div class="centered">
<img src="{{site.baseurl}}/res/ochaken/traces_ochaken2_back.png" alt=""/>
</div>

Great, now that pins match our expectations, the final question: Where to solder wires, before or after resistors?

If we go back to examples, we see that resistors sit between VCC and "the rest". Therefore, we should solder wires on resistor ends that are closer to SoC pins.

This is also correlated with a slight difference seen in voltage tests: On Beena, VCC is 3.3V, but JTAG signals are pulled up to 3.28V. On TV Ocha-Ken, the jumper pin is 3.28V, while resistors ends closer to the SoC are 3.25V.

Here's everything attached (including GND from a jumper pin):

<div class="centered">
<img src="{{site.baseurl}}/res/ochaken/dumper.jpg" alt=""/>
</div>

Afterwards, it was [OpenOCD shenanigans like previously covered](https://qufb.gitlab.io/writeups/2023/03/16/Climbing-the-Beenastalk#interfacing-with-openocd). Game ROM is fully memory-mapped, thus accessible with a debugger.

Since we didn't connect nSRST here, it was disabled in our configs.

9h00008.cfg:

```diff
-# reset_config trst_only
+reset_config trst_only
```

bbb.cfg:

```diff
-sysfsgpio_srst_num 65
+# sysfsgpio_srst_num 65
```

Quick check in MAME of the ROM dump:

<div class="centered">
<img src="{{site.baseurl}}/res/ochaken/emu.png" alt=""/>
</div>

# Closing thoughts

**Why not solder to SoC pins directly?** Because not only I lack those thin enameled wires, but also and crucially, lack the finesse for that job!

**Why not dump from flash ROM?** Although the marking isn't very readable in photos, by shining a flashlight at the right angle, we can tell it's a Mitsubishi M5M29GT320VP-80, there's even a [public datasheet](https://www.datasheetarchive.com/M5M29GT320-datasheet.html)! I just find JTAG to be easier for me.

<div class="centered" style="width: 16rem">
<img src="{{site.baseurl}}/res/ochaken/pcb_flash.png" style="display: inline-block" alt=""/>
</div>
