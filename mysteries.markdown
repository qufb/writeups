---
layout: post
index: 999
title: Mysteries...
platforms: ghost
---

## Giochi Preziosi My life

<div class="centered">
<img src="{{site.baseurl}}/res/mylife/IMG_2740.JPG" style="display: inline-block; width: 45%" alt=""/>
<img src="{{site.baseurl}}/res/mylife/IMG_2738.JPG" style="display: inline-block; width: 45%" alt=""/>
</div>

<div class="centered">
<img src="{{site.baseurl}}/res/mylife/IMG_2741.JPG" style="display: inline-block; width: 45%" alt=""/>
</div>

## Giochi Preziosi My Real Life

* [Patent](https://patents.google.com/patent/WO2010076819A1/en) suggests 32-bit RISC-based processor.

<div class="centered">
<img src="{{site.baseurl}}/res/mylife/IMG_2749.JPG" style="display: inline-block; width: 45%" alt=""/>
<img src="{{site.baseurl}}/res/mylife/IMG_2743.JPG" style="display: inline-block; width: 45%" alt=""/>
</div>

<div class="centered">
<img src="{{site.baseurl}}/res/mylife/IMG_2751.JPG" style="display: inline-block; width: 45%" alt=""/>
<img src="{{site.baseurl}}/res/mylife/IMG_2754.JPG" style="display: inline-block; width: 45%" alt=""/>
</div>

