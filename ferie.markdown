---
layout: post
index: 4
title: Sega Ferie
platforms: ferie
---

<p class="post-meta">Update 2024-02-18: Add test functions</p>
<p class="post-meta">Update 2024-03-24: Add Ferie Puppy pinout</p>

## Tooling

* [Ghidra loader](https://github.com/qufb/SegaFerieLoader)

## Dumping with universal programmer

ROM pinout is compatible with the more common Atmel AT27C040 or AT27C080 (if you discard upper half), so it can be dumped with any programmer that supports it (e.g. XGecu T56).

## Dumping with microcontroller board

I used a regular sized Arduino Mega 2560 along with my fork of cartreader ([branch `ferie`](https://github.com/qufb/cartreader/tree/ferie)), which is a minimal version that assumes ROM pins are connected directly via flying wires.

Follow the [pinout spreadsheet](https://github.com/qufb/cartreader/blob/ferie/pinout.ods) for Sega Master System:

|Arduino|ATMega2560|SMS|
|:--|:--|:--|
|A0|PF0|A0|
|A1|PF1|A1|
|A2|PF2|A2|
|A3|PF3|A3|
|A4|PF4|A4|
|A5|PF5|A5|
|A6|PF6|A6|
|A7|PF7|A7|
|A8|PK0|A8|
|A9|PK1|A9|
|A10|PK2|A10|
|A11|PK3|A11|
|A12|PK4|A12|
|A13|PK5|A13|
|A14|PK6|A14|
|D6|PH3|A15|
|D9|PH6|OE/RD|
|D30|PC7|D7|
|D31|PC6|D6|
|D32|PC5|D5|
|D33|PC4|D4|
|D34|PC3|D3|
|D35|PC2|D2|
|D36|PC1|D1|
|D37|PC0|D0|
|D48|PL1|CE|

Additionally, to cover remaining address pins, connect these custom assignments:

|Arduino|ATMega2560|SMS|
|:--|:--|:--|
|D10|PB4|A16|
|D11|PB5|A17|
|D12|PB6|A18|
|D13|PB7|A19|

Don't forget GND and VCC.

Use [Arduino IDE to upload](https://github.com/sanni/cartreader/wiki/How-to-flash-the-Arduino) `Cart_Reader_Ferie.ino`, then connect a serial console (e.g. `picocom -b 9600 /dev/ttyACM0`). You should see a "START" message. Press any key to setup pins and then start dumping read bytes to serial output. The program waits for input at every `0x10000` dumped bytes, so that you can manually copy them. Alternatively, you can remove these waits and try [logging to a file](https://askubuntu.com/questions/1010232/how-to-save-the-output-of-picocom-to-a-file).

## Test functions

AFAIK they are unused. Code exists to check string "TEST" at RAM address 0x70 after reset. But this address is only written to by mini-games, and none of them have any apparent way to write such bytes.

To load these functions on hardware, I [patched the main screen](https://gitlab.com/qufb/dump/-/blob/master/ferie/ferieki.pat.diff) in a flash ROM: Calculator function writes "TEST" in the expected memory address, and jumps to test code itself; Clock function only jumps to test code, and would likely be triggered just by resetting (since "TEST" is cleared by test code).

If "TEST" is found, we have a backup test with a hidden credits roll (enabled by pressing all 4 directional buttons + RET button at the same time; keeps playing in a loop).

<div class="centered">
<video width="720" height="480" controls>
<source src="{{site.baseurl}}/res/ferie/IMG_1750.mp4" type="video/mp4">
Your browser does not support the video tag.
</video>
</div>

If "TEST" is not found, we have another set of tests. You can tell my LCD screen is missing a vertical line, and also incorrectly draws 2 horizontal lines across the screen when drawing the rectangle.

Note that ROM test verifies contents against checksums at 0x7fff0. If any patching is done, these checksums must be updated as well (it's just a count of set bits in a 0x10000 sized page, which can be [computed with a script](https://gitlab.com/qufb/dump/-/blob/master/ferie/0x10000.py)). Curiously, the last checksum is wrong, but the test actually skips its page (0x70000..0x7ffff).

<div class="centered">
<video width="720" height="480" controls>
<source src="{{site.baseurl}}/res/ferie/IMG_1751.mp4" type="video/mp4">
Your browser does not support the video tag.
</video>
</div>

For flash ROM, I used SST39SF040. It's connected to Ferie via enameled wires soldered to an interposer board, with headers for conveniently connecting DuPont wires.

<div class="centered">
<img src="{{site.baseurl}}/res/ferie/IMG_1739.JPG" style="display: inline-block; max-width: 45%" alt=""/>
<img src="{{site.baseurl}}/res/ferie/IMG_1758.JPG" style="display: inline-block; max-width: 45%" alt=""/>
</div>

Note that SST39SF040's pinout has a slight difference: pin 1 is A18, while pin 31 is WE. Therefore, pin 31 (A18) on Ferie must be connected to pin 1 on flash ROM. I've also used pin 32 (VCC) to pull-high WE with a 4.7k resistor.

<div class="centered">
<img src="{{site.baseurl}}/res/ferie/IMG_1746.JPG" style="display: inline-block; max-width: 45%" alt=""/>
</div>

## PCB

### Kitty / Toyo Suisan

* Model: A 028019 / 1994
* PCB Revision: SB1P01B / 171-6896 / 837-11123
* Mask ROM: MPR-17062-T

<div class="centered">
<img src="{{site.baseurl}}/res/ferie/PCB_Front_Kitty.jpg" alt=""/>
</div>

### Puppy

* Model: E 035559 / 1995
* PCB Revision: MB-TPD338-1.0
* Mask ROM: Unknown (COBs only present in "E" models?)

<div class="centered">
<img src="{{site.baseurl}}/res/ferie/PCB_Front_Puppy.jpg" alt=""/>
<img src="{{site.baseurl}}/res/ferie/PCB_Back_Puppy.jpg" alt=""/>
</div>

By following GND + VCC, then all shared address + data lines between ROM and RAM, and figuring out other lines by relative positioning, I was able to verify that this ROM has the same pinout as packaged variants: 

<div class="centered">
<img src="{{site.baseurl}}/res/ferie/PCB_Front_Puppy_Pinout.png" style="display: inline-block; max-width: 45%" alt=""/>
<img src="{{site.baseurl}}/res/ferie/PCB_Back_Puppy_Pinout.png" style="display: inline-block; max-width: 45%" alt=""/>
</div>

Luckly, there are vias for all required lines, so there's no need to expose traces for soldering. To dump ROM in-circuit, programmers aren't able to supply the needed power to the whole circuit, thus triggering their built-in overcurrent protection. In this case, the board needs to be externally powered (2 AA batteries).

<div class="centered">
<img src="{{site.baseurl}}/res/ferie/IMG_1836.JPG" style="display: inline-block; max-height: 15rem" alt=""/>
<img src="{{site.baseurl}}/res/ferie/IMG_1839.JPG" style="display: inline-block; max-height: 15rem" alt=""/>
</div>

My initial approach (left pic) was to hold reset at power-on. Unfortunately this wasn't sufficient, since CPU still tried to access ROM, creating contention with programmer's accesses. The only way to get reliable dumps was to cut CPU traces 😥 (right pic).

### World Travel

* Model: C 019432 / 1995
* PCB Revision: SB1P02A
* Mask ROM: MPR-18080A

<div class="centered">
<img src="{{site.baseurl}}/res/ferie/PCB_Front_WorldTravel.jpg" alt=""/>
</div>
