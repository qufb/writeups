---
layout: post
index: 1
title: Reversing Notes
---

A compilation of resources and approaches I've taken to search and understand features of a given system. I don't claim to be any sort of authority in this matter, and there's probably more efficient alternatives out there. Take these as starting points to develop your own approaches.

## Methodology

We focus on **making state explicit**: annotating disassemblies, adding breakpoints and watches in a debugger, adding instrumentation for printfs, diffing trace logs...

Some domain-specific differences are worth pointing out:

- I/O data transfer (memory mapped registers vs. dedicated IN/OUT instructions);
- Memory model (flat vs. segmented / [multiple address spaces](https://github.com/qufb/SegaFerieLoader));
- Data formats (e.g. 2D tiles with indexed palettes, 3D meshes...);
- Code patterns (e.g. main loop, frame cap...);
    - Dynamic analysis via pure CPU emulation may not give the expected results due to missing logic happening outside the game's main loop (e.g. VBlank routine called via interrupt);

But before applying any approaches, let's avoid guessing: ask **"What is already known about what I will reverse?"**. For example, these are some questions I would make for the Playstation 1 console:

- General: Any public [documentation](https://psx.arthus.net/sdk/Psy-Q/DOCS/)? Or [development guides](http://psx.arthus.net/starting.html)? Or [security CTF writeups](https://gist.github.com/obskyr/99ce080f325bcc3d044f98fd90d447cb)?
- Architecture: What [CPU](https://problemkaputt.de/psx-spx.htm)? What [GPU](https://psx-spx.consoledev.net/graphicsprocessingunitgpu/)?
- File formats: These games come in CD-ROMs, which use a well-known filesystem (ISO9660), so we can expect executables and data to be split in files. Any [loader for executables](https://github.com/lab313ru/ghidra_psx_ldr)? How are these data files [open and read](https://psx.arthus.net/sdk/Psy-Q/DOCS/LIBREF46.PDF)?
    - Graphics: Are there well-known [texture and palette formats](http://www.psxdev.net/forum/viewtopic.php?t=109)? [Extraction tools](https://github.com/rickomax/psxprev/)? How would I [reverse some 3D model format](https://forum.xentax.com/viewtopic.php?p=161924)?
    - SDKs / toolchains / libraries: Are there [signatures made for labelling functions](https://github.com/lab313ru/psx_psyq_signatures) in the disassembly?
- Source code: Any games with leaked source code? Any [examples](https://github.com/jmiller656/PS1-Experimentation/blob/master/source/Main/main.c), either official or made by homebrew communities? Any [hardware validation tests](https://github.com/JaCzekanski/ps1-tests)?

## Hardware architectures

- pc
    - **msdos, windows**: Intel 8086 (32 bits a.k.a. x86, or 64 bits a.k.a. amd64)
    - **macos 7..9**: Motorola 68000
        - [GitHub \- dgelessus/mac\_file\_format\_docs: A collection of technical documentation and specifications about Apple\- and Mac\-related file formats](https://github.com/dgelessus/mac_file_format_docs)
    - **amiga**: Motorola 68000
        - [Reference Library](http://www.theflatnet.de/pub/cbm/amiga/AmigaDevDocs/)
        - [Documents \- iKod.se](https://www.ikod.se/download/documents/)
- consoles
    - **sms**: Zilog Z80
        - [Development \- SMS Power!](https://www.smspower.org/Development/Index)
    - **gb, gbc**: Sharp SM83 (~= Zilog Z80, Intel 8080)
        - [Pan Docs](https://gbdev.io/pandocs/)
        - [Dan Docs](https://shonumi.github.io/dandocs.html)
        - [GbdevWiki](https://gbdev.gg8.se/wiki/articles/Main_Page)
        - [GitHub \- gbdev/awesome\-gbdev: A curated list of Game Boy development resources such as tools, docs, emulators, related projects and open\-source ROMs\.](https://github.com/gbdev/awesome-gbdev)
    - **gba**: Sharp SM83 + ARM7TDMI, **nds**: NTR (== ARM7TDMI + ARM946E-S)
        - [GBATEK \- GBA/NDS Technical Info](http://problemkaputt.de/gbatek.htm)
    - **mini**: Minx (~= Seiko S1C88)
        - [Pokemon\-Mini\.net \- Hardware Documentation](https://www.pokemon-mini.net/documentation/)
    - **nes**: Ricoh 2A03 (~= MOS Technology 6502)
        - [NESdev Wiki](https://www.nesdev.org/wiki/Nesdev_Wiki)
        - [Category:Guides \- ROM Detectives Wiki](http://www.romdetectives.com/Wiki/index.php?title=Category:Guides)
    - **snes**: Ricoh 5A22 (~= Western 65C816)
        - [SNES Development Wiki \| Super Famicom Development Wiki](https://wiki.superfamicom.org/)
    - **tg16**: Hudson Soft HuC6280 (~= MOS Technology 6502, WDC 65C02)
        - [pcetech\.txt](https://github.com/MiSTer-devel/TurboGrafx16_MiSTer/blob/5f66eaffaf2365576c08636179ee3af3d079152f/docs/pcetech.txt)
        - [Archaic Pixels](http://www.archaicpixels.com/Main_Page)
    - **3do**: ARM60
        - [start \(3DO Development Repo\)](https://3dodev.com/)
    - **cdi**: Philips SCC68070 (~= Motorola 68000, e.g. trap instructions are different)
        - [The New International CD\-i Association](http://icdia.co.uk/)
    - **ngp**: Toshiba TLCS900H
        - [ngpcspec\.txt](http://devrs.com/ngp/files/DoNotLink/ngpcspec.txt)
    - **neo geo**: Motorola 68000
        - [AJ/freem's Neo\-Geo Development Page](https://ajworld.net/neogeodev/)
    - **md, mcd**: Motorola 68000
        - [sega2f](http://xi6.com/files/sega2f.html)
        - [Category:Mega Drive technical information \- Sega Retro](https://segaretro.org/Category:Mega_Drive_technical_information)
        - [System Documentation \(Sega Mega Drive\) \- techdocs.exodusemulator.com](http://techdocs.exodusemulator.com/Console/SegaMegaDrive/Documentation.html)
        - [GitHub \- drojaazu/megadev: A Sega Mega CD development framework in C and 68k asm](https://github.com/drojaazu/megadev)
    - **32x**: Hitachi SH7604 (a.k.a. SH-2)
        - [32Xtoc](https://mode5.net/32x-DDK/32Xtoc.html)
        - [GitHub \- viciious/32XDK: Development kit for the Sega 32X](https://github.com/viciious/32XDK)
    - **saturn**: Hitachi SH7604 (a.k.a. SH-2)
        - [Sega Saturn hardware notes \(2004\-04\-27\) \- Sega Retro](https://segaretro.org/Sega_Saturn_hardware_notes_(2004-04-27))
        - [Saturn documentation, libs &amp; stuff](antime.kapsi.fi/sega/docs.html)
    - **dc**: Hitachi SH7750 (a.k.a. SH-4)
        - [Development \- DCEmulation](https://dcemulation.org/index.php?title=Development)
        - [GitHub \- dreamcastdevs/awesome\-dreamcast: A collection of useful link and tool for Sega \#Dreamcast development](https://github.com/dreamcastdevs/awesome-dreamcast)
    - **n64**: NEC VR4300 (== MIPS R4300i)
        - [Nintendo 64 Development](http://n64dev.org/)
        - [en64 wiki](http://en64.shoutwiki.com/wiki/Main_Page)
    - **psx**: CoreWare CW33300 (== MIPS R3051)
        - [PSXSPX Specifications](http://problemkaputt.de/psx-spx.htm)
        - [Psy-Q Library Reference](https://psx.arthus.net/sdk/Psy-Q/DOCS/LIBREF46.PDF)
        - [ps1\-links](https://ps1.consoledev.net/)
        - [GitHub \- mateusfavarin/psx\-modding\-toolchain: A set of tools to mod and reverse engineer PSX games\.](https://github.com/mateusfavarin/psx-modding-toolchain)
    - **ps2**: Emotion Engine (== MIPS R5900)
        - [ps2tek \- Documentation on PS2 internals](https://psi-rockin.github.io/ps2tek/)
        - [PS2 Dev Wiki](https://playstationdev.wiki/ps2devwiki/index.php/Main_Page)
- calling conventions
    - [musl libc \- ABI Cheat Sheet](https://wiki.musl-libc.org/abi-cheat-sheet.html)
- endianess: LE vs. BE
- source / destination operand order
- memory map
    - **amiga**: [Address space, CIA registers, Chip registers](http://oscomp.hu/depot/amiga_memory_map.html)
    - **gbc**: [Memory Map \- Pan Docs](https://gbdev.io/pandocs/Memory_Map.html)
    - **nes**: [CPU memory map \- NESdev Wiki](https://www.nesdev.org/wiki/CPU_memory_map)
    - **snes**: [SMAS Memory Map \- SMW Central](https://www.smwcentral.net/?p=memorymap&game=smas&u=0&address=&sizeOperation=checklist.markdown3D&sizeValue=&region[]=regs&type=*&context=*&description=)
    - **md**: [Sega Mega Drive/Memory map \- Sega Retro](https://segaretro.org/Sega_Mega_Drive/Memory_map)
        ```
        $000000  $3FFFFF  Cartridge ROM/RAM $2000000-$23FFFFF
        $880000  $8FFFFF  32X cartridge ROM (first 512kB bank only)
        $900000  $9FFFFF  32X cartridge bankswitched ROM (any 512kB bank, controlled by 32X registers)
        $A10002  $A10003  Controller 1 data
        $A10004  $A10005  Controller 2 data
        $A10006  $A10007  Expansion port data
        $A10008  $A10009  Controller 1 control
        $A1000A  $A1000B  Controller 2 control
        $A1000C  $A1000D  Expansion port control
        $C00000  $C00001  VDP data port
        $C00002  $C00003  VDP data port (mirror)
        $C00004  $C00005  VDP control port
        $C00006  $C00007  VDP control port (mirror)
        $FF0000  $FFFFFF  68000 RAM
        ```
    - **saturn**:
        - [SH\-2CPU \- Yabause](https://wiki.yabause.org/index.php5?title=SH-2CPU)
        - [SMPC \- Yabause](https://wiki.yabause.org/index.php5?title=SMPC)
    - **psx**: [Memory map \- PSXSPX Specifications](http://problemkaputt.de/psx-spx.htm#memorymap)
        ```
        CACHE     1f800000  1f8003ff  0x400
        MCTRL1    1f801000  1f801023  0x24
        IO_PORTS  1f801040  1f80105f  0x20
        MCTRL2    1f801060  1f801063  0x4
        INT_CTRL  1f801070  1f801075  0x6
        RAM       80000000  8000ffff  0x10000
        CODE      80010000  801f8fff  0x1e9000
        RAM       801f9000  801fffff  0x7000
        ```

- [Architecture of Consoles \| A Practical Analysis - Rodrigo Copetti](https://www.copetti.org/writings/consoles/)
- [System-specific resources - emudev.org](https://emudev.org/system_resources)
- [Documents - Romhacking.net](http://www.romhacking.net/documents/)
- [Documents - Zophar's Domain](https://www.zophar.net/documents.html)

### CPU debuggers

- pc
    - **x32dbg**, **x64dbg**, **ollydbg** (32bit, 95/98 host), **Open Watcom C/C++ Debugger** (16bit): windows
    - **DOSBox Debugger**: msdos
    - **Altirra**: atari
    - [**The Debugger**](https://www.jasik.com/features.html): macos 7..9
    - [**MacsBug**](https://smfr.org/computing/archaic/macsbug.html): macos 7..9
- arcade
    - [**MAME**](https://dorkbotpdx.org/blog/skinny/use_mames_debugger_to_reverse_engineer_and_extend_old_games/)
        - [Guides:MAME \- Troubleshooting \- Wah!ki](https://wahki.mameau.com/index.php?title=Guides:MAME_-_Troubleshooting)
        - [MAMEWorld Forums \- EmuChat \- MAME scripty things!](https://www.mameworld.info/ubbthreads/showflat.php?Cat=&Number=392265&page=0&view=expanded&sb=5&o=&vc=1)
        - [Show Your Working: Fixing a pile of bugs in MAME](https://moral.net.au/writing/2017/12/03/mame_fixes/)
- consoles
    - **ares** (trace logger): nes, snes, n64, sgg, sms, md, 32x, mcd, gb, pce, pcfx, psx, wswan, ngp, msx
    - [**mednafen**](https://mednafen.github.io/documentation/debugger.html) (trace logger): nes, md, pce, pcfx, psx, ss, vb, wswan
    - **FCEUX-DSP**, [**Mesen**](https://github.com/NovaSquirrel/Mesen-SX): nes
    - **BGB** (wine), **binjgb** (trace logger): gb
    - **mgba** (gdb remote, trace logger): gba
    - [**DeSmuME**](http://wiki.desmume.org/index.php?title=Faq#How_do_I_enable_the_GDB_stub.3F) (gdb remote): nds, [**melonDS**](https://github.com/melonDS-emu/melonDS/pull/1583) (gdb remote): nds, [**No$gba**](https://problemkaputt.de/gba-dev.htm) (wine): gba, nds
    - **BizHawk** (mono): sgg, sms, md (genesis plus gx), 32x, mcd, nes, snes, n64, gb, psx, ss, vb, wswan
    - **Emulicious**: gb, gbc, sms, sgg, msx
    - [**MEKA**](https://raw.githubusercontent.com/ocornut/meka/master/meka/debugger.txt): sg1000, sc3000, sms, sgg, colecovision
    - **bsnes-plus**: snes
    - **BlastEm** (gdb remote), **Exodus** (wine), **Gens Kmod** (wine, gdb remote), [**Gens 11 r57shell mod**](https://gendev.spritesmind.net/forum/viewtopic.php?t=1695#p22459) (wine, trace logger): md
    - [**Redream**](https://www.obscuregamers.com/threads/how-to-setup-gdb-debugging-in-linux-with-redream.1086/) (gdb remote via tty), [**Flycast dbgnet fork**](https://github.com/lhsazevedo/flycast/tree/dbgnet): dc
    - [**mupen64plus**](https://github.com/mupen64plus/mupen64plus-ui-console): n64
    - **Dolphin**: gamecube
    - [**PCSX-Redux**](https://pcsx-redux.consoledev.net/gdb-server/) (gdb remote): psx
    - [**PCSX2**](https://github.com/PCSX2/pcsx2/blob/master/bin/docs/debugger.txt): ps2

- [Assembly Hacking \- Wiki \- GameHacking\.org](https://wiki.gamehacking.org/Assembly_Hacking)

### CPU disassemblers

- radare2
    ```sh
    echo 0800428c | xargs -I{} rasm2 -a mips -b 32 -d {}
    # lw v0, 8(v0)
    ```
- MAME: [unidasm](https://docs.mamedev.org/tools/othertools.html?highlight=unidasm) or skeleton driver that loads ROM and attaches CPU
    ```sh
    make REGENIE=1 TOOLS=1

    echo 0800428c | xxd -r -p > /tmp/1 && ./unidasm /tmp/1 -arch mips3le
    # 0: 8c420008  lw        $v0,$8($v0)

    # rebuild subset of drivers
    make SUBTARGET=foo SOURCES=src/mame/drivers/foo.cpp
    ```
- [The Ultimate Disassembly Framework – Capstone – The Ultimate Disassembler](https://www.capstone-engine.org/)

### CPU emulators

- multiarch
    - [GitHub \- unicorn\-engine/unicorn: Unicorn CPU emulator framework \(ARM, AArch64, M68K, Mips, Sparc, X86\)](https://github.com/unicorn-engine/unicorn)
    - [GitHub \- Nalen98/GhidraEmu: Native Pcode emulator](https://github.com/Nalen98/GhidraEmu)
- tiny
    - [GitHub \- cnlohr/mini\-rv32ima: A tiny C header\-only risc\-v emulator\.](https://github.com/cnlohr/mini-rv32ima)
    - [GitHub \- jart/blink: tiniest x86\-64\-linux emulator](https://github.com/jart/blink)
    - [GitHub \- adriancable/8086tiny: Official repository for 8086tiny: a tiny PC emulator/virtual machine](https://github.com/adriancable/8086tiny)

#### Debugging the emulator

One of the challenges is in figuring out how the emulated system's memory maps are allocated in the emulator's process memory maps, for which a [byte signature can be used for translating addresses](https://www.abartel.net/static/p/hacklu2019_build_engine.pdf).

#### Patching the emulator

- [Cracking Crash Bandicoot's password algorithm](https://news.ycombinator.com/item?id=9373599)
    - [crash\-bandicoot\-password\-cracking/pcsx\-1\.9\.92\-vm\-dump\.patch at master · dezgeg/crash\-bandicoot\-password\-cracking · GitHub](https://github.com/dezgeg/crash-bandicoot-password-cracking/blob/master/pcsx-1.9.92-vm-dump.patch)
- [\(FR\) Write\-up FCSC 2021 : Stars:2ndMix CryptoEdition \- redoste](https://redoste.xyz/2021/05/03/fr-write-up-fcsc-2021-stars2ndmix-cryptoedition/)

## Binary patching

- [Hack64 \- Web Patcher](https://hack64.net/tools/patcher.php)
- [High level mods/patches for console video\-games](https://krystalgamer.github.io/high-level-game-patches/)
- [IPS File Format \- :: ZeroSoft ::](https://zerosoft.zophar.net/ips.php)
- GameGenie
    - **nes**
        - [Technical Notes](http://tuxnes.sourceforge.net/gamegenie.html)
    - [Game Genie Encoder/Decoder](https://games.technoplaza.net/ggencoder/js/)
- GameShark
    - **gba**
        - [GBATEK GBA Cheat Codes \- Gameshark/Action Replay V1/V2](http://problemkaputt.de/gbatek-gba-cheat-codes-gameshark-action-replay-v1-v2.htm)
        ```
        0aaaaaaa 000000xx  [aaaaaaa]=xx
        1aaaaaaa 0000xxxx  [aaaaaaa]=xxxx
        2aaaaaaa xxxxxxxx  [aaaaaaa]=xxxxxxxx
        3000cccc xxxxxxxx  write xxxxxxxx to (cccc-1) addresses (list in next codes)
        aaaaaaaa aaaaaaaa  parameter for above code, containing two addresses each
        aaaaaaaa 00000000  last parameter for above, zero-padded if only one address
        60aaaaaa y000xxxx  [8000000h+aaaaaa*2]=xxxx (ROM Patch)
        8a1aaaaa 000000xx  IF GS_Button_Down THEN [a0aaaaa]=xx
        8a2aaaaa 0000xxxx  IF GS_Button_Down THEN [a0aaaaa]=xxxx
        80F00000 0000xxxx  IF GS_Button_Down THEN slowdown xxxx * ? cycles per hook
        Daaaaaaa 0000xxxx  IF [aaaaaaa]=xxxx THEN (next code)
        E0zzxxxx 0aaaaaaa  IF [aaaaaaa]=xxxx THEN (next 'zz' codes)
        Faaaaaaa 00000x0y  Enable Code - Hook Routine
        xxxxxxxx 001DC0DE  Enable Code - Game Code ID (value at [0ACh] in cartridge)
        DEADFACE 0000xxyy  Change Encryption Seeds
        ```
    - **psx**
        - [Cheat Devices \- ActionReplay/GameShark Code Format \- PlayStation Development Network](http://www.psxdev.net/forum/viewtopic.php?t=495)
        ```
        1-byte rewrite: 30xxxxxx yy
        2-byte rewrite: 80xxxxxx yyyy
        if [xxxxxx] == yyyy, then execute next code: D0xxxxxx yyyy
        if [xxxxxx] != yyyy, then execute next code: D1xxxxxx yyyy
        if [xxxxxx] <  yyyy, then execute next code: D2xxxxxx yyyy
        if [xxxxxx] >= yyyy, then execute next code: D3xxxxxx yyyy
        ```
    - **saturn**
        - [Code Types \(Sega Saturn\) \- Wiki \- GameHacking\.org](https://wiki.gamehacking.org/Code_Types_(Sega_Saturn))
        ```
        Writes YYYY to address XXXXXXX (only once upon bootup): 0XXXXXXX YYYY
        Writes YYYY to address XXXXXXX: 1XXXXXXX YYYY
        Writes   YY to address XXXXXXX: 3XXXXXXX 00YY
        If value at XXXXXXX is equal to YYYY, then execute the next code: DXXXXXXX YYYY
        ```
    - [The Secrets of Professional GameShark\(tm\) Hacking](https://gamehacking.org/faqs/hackv500c.html)
    - [GBA AR Crypt \- RAM Editing \- Project Pokemon Forums](https://projectpokemon.org/home/files/file/2134-gba-ar-crypt/)

## ASM patterns

- code not automatically disassembled
    - Ghidra > Analysis Options > Enable "Agressive Instruction Finder"
    - run in tracing disassembler: e.g. [Link’s Awakening disassembly progress report – part 7 \| Kzone](https://kemenaran.winosx.com/posts/links-awakening-disassembly-progress-report-part-7)
    - lookup function prologue and epilogue
        - **m68k**: `rts` (4e 75)
        - **arm**: e.g. [Godzilla Vs\. Kong Vs \.\.\. Ghidra? \- Ghidra Scripting, PCode Emulation, and Password Cracking \| Wrongbaud's Blog](https://wrongbaud.github.io/posts/kong-vs-ghidra/)
- data without xrefs
    - set hardware read breakpoints
    - lookup addresses / offsets
        - **x86**: relative offsets
            ```
            ; hex((0x37 + 2) + 0x12) = 0x4b
            140001437 75 12           JNZ        LAB_14000144b
            ```
        - **m68k**: find `bra` bytes (60 00), take relative offset bytes (XX XX), add them to instruction address, check which fall into a given target address
        - **mips**: high 2 bytes loaded first, low 2 bytes loaded next
            ```
            800247f4 0d 80 04 3c     lui        a0,0x800d
            800247f8 b0 f3 84 8c     lw         a0,-0xc50(a0)=>DAT_800cf3b0
            ```
        - **sh-2**: addresses stored in memory table after end of consumer function, accessed by $pc offset
            ```
            ; hex((0x6a + 2) + 0x20 * 4) = 0xec
            06006d6a d1 20           mov.l      @(PTR_PTR_DAT_06006dec,pc),r1
            [...]
            06006de6 00 0b           rts
            06006de8 6e f6           _mov.l     @r15+,r14
            06006dea 00              ??         00h
            06006deb 00              ??         00h
                                 PTR_PTR_DAT_06006dec     XREF[1]:  FUN_06006d68:06006d6a(R)
            06006dec 06 00 40 d4     addr       PTR_DAT_060040d4  = 060e7106
            ```
- indirection:
    - **x86**: e.g. [Another kept egg out of the dungeon](2021/05/05/Another-kept-egg-out-of-the-dungeon)
    - **m68k**: e.g. _Marble Madness (Tengen, Genesis)_
        ```
        00003c7c 30 38 d0 34     move.w     (DAT_ffffd034).w,D0w                             = ??
        00003c80 e5 48           lsl.w      #0x2,D0w
        00003c82 4e fb 00 04     jmp        (0x3c88,PC,D0w*0x1)
                             LAB_00003c86                                    XREF[1]:     00003c7a(j)
        00003c86 4e 75           rts
        00003c88 60 00 00 a2     bra.w      LAB_00003d2c
        00003c8c 60 00 01 1a     bra.w      LAB_00003da8
        00003c90 60 00 01 6c     bra.w      LAB_00003dfe
        ```
- strings
    - [CaH4e3's Stuff \- gbx_bgm_test.txt](http://cah4e3.shedevr.org.ru/misc/gbx_bgm_test.txt)
        ```sh
        LANG=C grep -Pobari '(bgm|\x82\x61\x82\x66\x82\x6c|music|\x82\x6c\x82\x74\x82\x72\x82\x68\x82\x62|sound|\x82\x72\x82\x6e\x82\x74\x82\x6d\x82\x63|sfx|pcm).*' foo.bin
        LANG=C grep -Pobari '((level|stage).select)|test.mode|cheat|debug' foo.bin
        ```
    - [User:Andlabs/DEBUG \- The Cutting Room Floor](https://tcrf.net/User:Andlabs/DEBUG)
        ```sh
        LANG=C grep -Poba '(\x82\x63\x82\x64\x82\x61\x82\x74\x82\x66)|(\x82[\x63\x84]\x82\x85\x82\x82\x82\x95\x82\x87)|(\x83\x66\x83\x6F(\x83\x62)?\x83[\x4e\x4f])' foo.bin
        ```
    - [Kumon no Sukusuku Lesson 1\-kara 30\-made no Suuji \- The Cutting Room Floor](https://tcrf.net/Kumon_no_Sukusuku_Lesson_1-kara_30-made_no_Suuji)
        - "サウンドテスト" = Sound Test
    - [Princess Maker \(PlayStation 2\) \- The Cutting Room Floor](https://tcrf.net/Princess_Maker_(PlayStation_2))
        - "おんがく" = Music Player / Sound Test
    - [Sexy Parodius \- Debug Tool \| SUDDENﾃﾞｽ](https://sudden-desu.net/entry/sexy-parodius-debug-tool)
        - "[MUTEKI](https://jisho.org/search/muteki) (むてき) ON" = invincibility
        ```sh
        LANG=C grep -Poba '\x82\xde\x82\xc4\x82\xab' foo.bin
        ```
    - [\(REVERSE ENGINEERING\) Finding the correct encoding for hiragana text \| GBAtemp\.net \- The Independent Video Game Community](https://gbatemp.net/threads/reverse-engineering-finding-the-correct-encoding-for-hiragana-text.528959/)
        - `[\x00-\x7f]\xff`: latin
        - `[\x00-\x7f]\x30`: hiragana
- button codes
    - words
        - [Cheats in MediEvil \| Gallowmere Historia \| Fandom](https://gallowmere.fandom.com/wiki/MediEvil/Cheats)
        > "DUST LUST": Hold L2 and press DOWN, UP, SQUARE, TRIANGLE, LEFT, UP, SQUARE, TRIANGLE.
    - pad checks
        - [Found a Japanese Debug Menu (Genesis) - Forums - GameHacking.org](https://gamehacking.org/vb/forum/video-game-hacking-and-development/retro-hacking/4012-found-a-japanese-debug-menu-genesis?p=68491#post68491)
            ```
            lui v1, $800b
            addiu v1, v1, $5828 ; P1 Normal Joker command = 800B5828
            andi v0, v0, $0900 ; Select + Start
            bne v0, zero, $8002b7c4
            ```
        - [Marble Madness Cheats, Codes, and Secrets for Game Boy Color \- GameFAQs](https://gamefaqs.gamespot.com/gbc/576088-marble-madness/cheats)
            ```
            rom2::440e 21 04 cb  LD  HL,0xcb04
            rom2::4411 2a        LD  A,(HL+)=>DAT_cb04
            rom2::4412 fe 40     CP  0x40
            rom2::4414 20 46     JR  NZ,LAB_rom2__445c
            rom2::4416 2a        LD  A,(HL+)=>DAT_cb05
            rom2::4417 fe 80     CP  0x80
            rom2::4419 20 41     JR  NZ,LAB_rom2__445c
            rom2::441b 2a        LD  A,(HL+)=>DAT_cb06
            rom2::441c fe 20     CP  0x20
            rom2::441e 20 3c     JR  NZ,LAB_rom2__445c
            rom2::4420 2a        LD  A,(HL+)=>DAT_cb07
            rom2::4421 fe 10     CP  0x10
            rom2::4423 20 37     JR  NZ,LAB_rom2__445c
            ```
    - xorshift
        - [Notes:Alien Storm \(Genesis\) \- The Cutting Room Floor](https://tcrf.net/Notes:Alien_Storm_(Genesis))
- debug displays
    - format strings
        ```sh
        grep -riwoba '[ :=]%\(0[0-9]\)\?[cdsx]'
        ```
    - hex charsets
        - e.g. [Desert Strike: Return to the Gulf \(SNES\) \- The Cutting Room Floor](https://tcrf.net/index.php?title=Desert_Strike:_Return_to_the_Gulf_(SNES)&oldid=95023)
        ```sh
        grep -rioba '0123456789abcdef[^Gg]'
        ```
- offset tables
    - e.g. [Help:Contents/Finding Content/Genesis research guide \- The Cutting Room Floor](https://tcrf.net/Help:Contents/Finding_Content/Genesis_research_guide)
- signatures
    > This function, up until $8526, doesn't depend on addressing or offsets for its opcodes and so the raw byte code that represents the first ten instructions of this subroutine can be used in combination of a "binary search" to find other ROM files that utilize the same function.
- [Help:Contents/Finding Content \- The Cutting Room Floor](https://tcrf.net/Help:Contents/Finding_Content)

## Data formats

- **psx**
    - [Playstation 1 File Formats \- RetroReversing](https://www.retroreversing.com/ps1-file-formats)
    - [ELF2EXE](https://github.com/cetygamer/psxsdk/blob/master/tools/elf2exe.c)
- [Game File Format Central \- XentaxWiki](http://wiki.xentax.com/index.php/Game_File_Format_Central)
    - [The Definitive Guide To Exploring File Formats \- XentaxWiki](http://wiki.xentax.com/index.php/DGTEFF)
- [Category:Game Formats \- MultimediaWiki](https://wiki.multimedia.cx/index.php?title=Category:Game_Formats)
- [ModdingWiki](http://www.shikadi.net/moddingwiki/Main_Page)
- [REWiki](https://web.archive.org/web/20180127174342/http://rewiki.regengedanken.de/wiki/Main_Page)
- [Chokuretsu ROM Hacking Challenges Part 1 – Cracking a Compression Algorithm! \- Haroohie Translation Club Blog](https://haroohie.club/blog/2022-10-19-chokuretsu-compression/)
- [Chokuretsu ROM Hacking Challenges Part 2 – Archive Archaeology \- Haroohie Translation Club Blog](https://haroohie.club/blog/2022-11-02-chokuretsu-archives/)

### Graphics

- formats
    - **gbc**: [Gameboy 2BPP Graphics Format](https://www.huderlem.com/demos/gameboy2bpp.html)
    - **dc**: [PVR Graphic Formats \(Dreamcast 2D Graphics\) by SmokesGrass](https://fabiensanglard.net/Mykaruga/tools/segaPVRFormat.txt)
- compression
    - **nes**: [Tile compression \- NESdev Wiki](https://www.nesdev.org/wiki/Tile_compression)
    - **n64**: [MIO0 Compressed Data Format \- skelux\.net](http://skelux.net/showthread.php?tid=113)
    - **sms**: [Compression \- Development \- SMS Power!](https://www.smspower.org/Development/Compression)
    - **md**: [Category:Data compression \- Sega Retro](https://segaretro.org/Category:Data_compression)
    - [Category:Compression algorithms \- ModdingWiki](https://moddingwiki.shikadi.net/wiki/Category:Compression_algorithms)
    - [Pokémon Sprite Decompression Explained \- YouTube](https://www.youtube.com/watch?v=aF1Yw_wu2cM)
- viewers
    - [YY\-CHR @wiki \- atwiki（アットウィキ）](https://w.atwiki.jp/yychr/)
    - [GitHub \- bbbradsmith/binxelview: Binxelview binary image explorer](https://github.com/bbbradsmith/binxelview)
- parsers
    - [GitHub \- drojaazu/chrgfx: Converts to and from tile based graphics from retro video game hardware](https://github.com/drojaazu/chrgfx)
    - [GitHub \- bbbbbr/gimp\-rom\-bin: GIMP plug\-in to read/write SNES / NES / GB / GBA / NGP / MD / etc ROM image, tile and sprite files](https://github.com/bbbbbr/gimp-rom-bin)
- font tiles loaded using order of required characters to be displayed
    - **md**: e.g. _Asterix and the Great Rescue_
        - dumped VRAM open in YY-CHR
            ![](res/checklist/asterix.png)
        - tiles displayed referenced by index of tiles loaded (starts at 0x36e in dump, 0xff036e in VRAM)
            ```
            00000360: 7333 7f33 3333 8833 3333 f733 3333 0001  s3.333.333.333..
            00000370: 0002 0003 0004 0005 0003 0006 0007 0004  ................
            00000380: 0008 0009 0004 000a 000b 0004 000c 000d  ................
            00000390: 000e 0004 000f 0006 0010 0011 0004 0008  ................
            ```
- tile block mappings
    - **gbc**: [Reverse Engineering Carrot Crazy \- Part 3 \- Level Maps \| Shanty Blog](https://www.huderlem.com/blog/posts/carrot-crazy-3/)
    - **md**: [SCHG:Sonic the Hedgehog \(16\-bit\)/Level Editing \- Sonic Retro](http://info.sonicretro.org/SCHG:Sonic_the_Hedgehog_(16-bit)/Level_Editing)
- lookup addresses from VRAM patterns
    > locate the uncompressed VRAM data in an emulators save state in TLP, then search for those hex strings in the main rom
    - [Hacking a game that isn&\#039;t Sonic from scratch \| Sonic and Sega Retro Forums](https://forums.sonicretro.org/index.php?threads/hacking-a-game-that-isnt-sonic-from-scratch.31329/#post-742199)
- hook via dll
    - [Thyme \- hooker.h](https://github.com/TheAssemblyArmada/Thyme/blob/develop/src/hooker/hooker.h)
    ```cpp
    inline void Hook_Func(uintptr_t in, uintptr_t out)
    {
    #ifdef GAME_DLL
        static_assert(sizeof(x86_jump) == 5, "Jump struct not expected size.");

        x86_jump cmd;
        cmd.addr = out - in - 5;
        WriteProcessMemory(GetCurrentProcess(), (LPVOID)in, &cmd, 5, nullptr);
    #endif
    }
    ```
- hook via dynamic instrumentation
    - [Adding new features to an old game with Frida, Part I \| erfur’s bits and pieces](https://erfur.github.io/2022/07/16/mysterypi-frida-pt1.html)
    ```javascript
    rpc.exports = {
        checkDisplay() {
            var ctx = getContext();
            send({
                context: ctx,
                fullScreenFlag: ptr(0x004ECCC6).readU8(),
            })
        },
        setDisplay(flag) {
            var ctx = getContext();
            var setDisplay = new NativeFunction(ptr(0x401a50), "void", ["uint8", "pointer"]);
            var setFullscreen = new NativeFunction(ptr(0x40fc50), "void", ["pointer", "uint8"], "thiscall");

            setDisplay(flag, ctx);
            setFullscreen(ctx, flag);
        }
    }
    // On python:
    // api = script.exports
    // api.set_display(int(cmd[1]))
    ```
- [Extracting simple models \- XeNTaX](https://forum.xentax.com/viewtopic.php?t=10894)

## Emulator dev

- high-level tests
    - [GitHub \- ArtemioUrbina/240pTestSuite: A homebrew software suite for video game consoles developed to help in the evaluation of upscalers, upscan converters, line doublers and of course TV processing of 240p video\. The Wii and Dreamcast versions have modes for 480i and 480p evaluation as well\.](https://github.com/ArtemioUrbina/240pTestSuite)
    - [GitHub \- pinobatch/240p\-test\-mini: Size\-optimized ports of Artemio&\#39;s 240p Test Suite to 8\-bit consoles](https://github.com/pinobatch/240p-test-mini)
    - [GBEmulatorShootout](https://daid.github.io/GBEmulatorShootout/)
    - [Gameboy Doctor: debug and fix your gameboy emulator \| Robert Heaton](https://robertheaton.com/gameboy-doctor/)
    - [GitHub \- destoer/armwrestler\-gba\-fixed: armwrestler for gba with multiple load\-store tests working](https://github.com/destoer/armwrestler-gba-fixed)
    - [SNES/CPUADC\.asm at master · PeterLemon/SNES · GitHub](https://github.com/PeterLemon/SNES/blob/master/CPUTest/CPU/ADC/CPUADC.asm)
    - [z80emu/zexall\.z80 at master · anotherlin/z80emu · GitHub](https://github.com/anotherlin/z80emu/blob/master/testfiles/zexall.z80)
- cycle / timing tests
    - [Emulator tests \- NESdev Wiki](https://www.nesdev.org/wiki/Emulator_tests)
    - [blargg's SPC test ROMs \- NESdev BBS](https://archive.nes.science/nesdev-forums/f12/t18005.xhtml)
    - [GitHub \- VitorVilela7/SnesSpeedTest: Homebrew Super Nintendo ROM for measuring SNES and SA\-1 clock speed\.](https://github.com/VitorVilela7/SnesSpeedTest)
    - [Development Tools/Software \(Sega Mega Drive\)](http://techdocs.exodusemulator.com/Console/SegaMegaDrive/Software.html#test-roms)
    - [PC\-Engine CPU Test](https://chrismcovell.com/CPUTest/index.html)
- transistor-level emulation
    - [GitHub \- floooh/v6502r: visual6502 remixed](https://github.com/floooh/v6502r)
- [Simple VM \- CodeGuppy Playground](https://codeguppy.com/code.html?t=simple_vm)
- [How to write an emulator \(CHIP\-8 interpreter\) \| Multigesture\.net](https://multigesture.net/articles/how-to-write-an-emulator-chip-8-interpreter/)
- [Emulating the Chip8 system \- codeslinger\.co\.uk](http://www.codeslinger.co.uk/pages/projects/chip8.html)
- [Emulating the Sega Genesis \- Part I](https://jabberwocky.ca/posts/2022-01-emulating_the_sega_genesis_part1.html)
- [POKEGB: a gameboy emulator that only plays Pokémon blue](https://binji.github.io/posts/pokegb/)
- [emudev \- developing emulators](https://emudev.de/)

## ROM dumping

### Tutorials

- [The Guru's ROM Dump News \- How to Dump ROMs](https://web.archive.org/web/20230406133903/http://members.iinet.net.au/~lantra9jp1_nbn/gurudumps/tutorials/how_to_dump/)
- [The Guru's ROM Dump News \- How to Remove Soldered-in ROMs](https://web.archive.org/web/20221207004415im_/http://members.iinet.net.au/~lantra9jp1_nbn/gurudumps/tutorials/how_to_remove/index.html)
- [Flash Dumping \- Part I](https://blog.quarkslab.com/flash-dumping-part-i.html)
- [Flash Dumping \- Part II](https://blog.quarkslab.com/flash-dumping-part-ii.html)
- [Category:Dumping Guides \- No\-Intro ~ Wiki](https://wiki.no-intro.org/index.php?title=Category:Dumping_Guides)

### Docs

- [Bitsavers \- Index of /components](http://www.bitsavers.org/components/)
- [The 'Wiretap' Arcade Game Collector's Archive](https://web.archive.org/web/20030802215906/http://www.spies.com/~arcade/)
- [Antelope Arcade File Archive](https://www.antelopearcade.com/files/)

### Decap

- [Silicon Pr0n - Decapsulation](https://siliconpr0n.org/wiki/doku.php?id=decap:start)
- [chips-howto \- Методы исследования](https://github.com/emu-russia/chips-howto/blob/main/methods.md)
- [Так какой же процессор использовался в играх Brick Game? Часть 2 / Хабр](https://habr.com/ru/articles/773040/)
- [Reverse engineering an LCD display](https://sudhir.nl/lcd?al=projects)

### Tools

- [GitHub \- sanni/cartreader: A shield for the Arduino Mega that can back up video game cartridges\.](https://github.com/sanni/cartreader)
- [Paul Molloy / INL\-retro\-progdump · GitLab](https://gitlab.com/InfiniteNesLives/INL-retro-progdump)

### Examples

- [Bus Sniffing the IBM 5150: Part 1](https://martypc.blogspot.com/2023/10/bus-sniffing-ibm-5150.html)
- [Software interrupt $1F / MidiKey2Freq](http://web.archive.org/web/20230321185310/https://gist.github.com/modwizcode/b4afc78ea74fb453be3bcaf3d3bc8adc)
- [Cracking the GBA BIOS \- mGBA](https://mgba.io/2017/06/30/cracking-gba-bios/)
- [Dumping the GBA BIOS via executing unmapped memory](https://web.archive.org/web/20240118000414if_/https://mary.rs/lab/gbabios/)
- [Dumping the boot ROM of the Gameboy clone Game Fighter](https://blog.gg8.se/wordpress/2014/12/09/dumping-the-boot-rom-of-the-gameboy-clone-game-fighter/)
- [PokéWalker hacking \- Dmitry\.GR](http://dmitry.gr/?r=05.Projects&proj=28.%20pokewalker)
- [VMU hacks \(&C\-M23 emulator\) \- Dmitry\.GR](http://dmitry.gr/index.php?r=05.Projects&proj=25.+VMU+Hacking)
- [Furrtek\.org : Dumping the NEC PC Engine YUV table](http://furrtek.free.fr/?a=pceyuv)
- [NMK004 ROM Dumping, Part 3: The Previous \| Daifukkat\.su](http://daifukkat.su/blog/archives/2014/09/09/nmk004_rom_dumping_part_3_the_previous/)
    > I concluded that I should try to play out the internal ROM with one of the tables, record the outputs, then analyze the sound files to recover the internal ROM. Within a few hours I had cooked up a sample ROM that attempted to play things out from the internal ROM. I did a short test in MAME just to verify that it would make sound, and it did. From there I made a reference ROM that I could use to compare the various values. By this time I had decided on using PSG note length as my dumping table, due to its long spread and easy reconstruction method.
- [MAME - emulating a handheld console, with ~20 licensed games from 2014 \(videos included\)](https://www.reddit.com/r/emulation/comments/wv4j1d/mame_emulating_a_handheld_console_with_20/)
    > One of the biggest initial challenges was also that the AX208 has an internal boot 8KB ROM which needed to be extracted in order to even start making progress.
    > This internal boot ROM / BIOS, like so many others, was secured against simple reading out, even with a software attack, but was vulnerable to the same kind of hole that allowed the GBA BIOS to be dumped, namely that code inside the BIOS area could read from the BIOS area, even if code executed outside of it could not. Some code modifications to the game ROM, and Peter Wilhelmsen was able to coax the internal ROM out by spamming data to the DAC audio channel we’d identified while studying the code, essentially using a morse code like protocol of long and short pulses to dump out the ROM.
- [Casio PB\-1000 \- internal ROM of the HD61700 processor](http://www.pisi.com.pl/piotr433/pb1000re.htm)
    > I managed to extract the ROM contents by single-stepping the code (by applying an IRQ2 interrupt pulse during execution of each instruction).
    > For this purpose all bus signals have been lead out to a connector inserted into a test socket of an universal device programmer TopMax controlled by a PC. With this equipment I can execute any single instruction, modify and examine the register and memory contents, log all bus activities, count cycles. The instructions can then be attempted to be identified.
- [Dumping Namco System 10 decrypted program code](https://twitter.com/_987123879113/status/1651986763198636032)
    - [decrypter\-serial\.asm · GitHub](https://gist.github.com/987123879113/c6e8eacd120012887e85db0efe30e988)
- [Flash cartridge Projects by Gerry O'Brien \- Development \- SMS Power!](https://www.smspower.org/Development/FlashCartridgeProjectsByGerryOBrien)
- [JAMMArcade\.net](https://jammarcade.net/)

## Exploit dev

### HW

- [GitHub \- sergiopolog/tlcs90\-rom\-reader: TLCS\-90 ROM Reader](https://github.com/sergiopolog/tlcs90-rom-reader)
- [GitHub \- racerxdl/stm32f0\-pico\-dump: STM32F0x Protected Firmware Dumper with Raspberry Pi Pico](https://github.com/racerxdl/stm32f0-pico-dump)

### SW

- [Hacking TMNF: Part 1 \- Fuzzing the game server \| bricked\.tech](https://blog.bricked.tech/posts/tmnf/part1/)
- [mast1c0re: Hacking the PS4 / PS5 through the PS2 Emulator \- Part 1 \- Escape](https://cturt.github.io/mast1c0re.html)
- [Hacking the Nintendo DSi Browser \| farlow\.dev](https://farlow.dev/2023/03/02/hacking-the-nintendo-dsi-browser)
- [Tetsuji: Remote Code Execution on a GameBoy Colour 22 Years Later :: TheXcellerator](https://xcellerator.github.io/posts/tetsuji/)
- [Finding and exploiting hidden features of Animal Crossing’s NES emulator \| jamchamb’s blog](https://jamchamb.net/2018/07/11/animal-crossing-nes-emulator-hacks.html)
