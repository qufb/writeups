#!/usr/bin/env python3

import sys


def chunks(lst, n):
    for i in range(0, len(lst), n):
        yield lst[i : i + n]


def update(reg):
    for i in range(8):
        hibit = ((reg & 1) ^ ((reg & 2) >> 1)) << 15
        reg = (hibit | reg) >> 1
    return reg


def scramble(data):
    reg = 1
    for i in range(4):
        reg = update(reg)
    for i in range(len(data)):
        data[i] = data[i] ^ (reg & 0xFF)
        reg = update(reg)
    return data


if __name__ == "__main__":
    filename = sys.argv[1]
    with open(filename, "rb") as f:
        data = f.read()
        for chunk in chunks(data, 0x800):
            descrambled = scramble(list(chunk))
            with open(filename + ".descrambled.bin", "ab") as f2:
                f2.write(bytes(descrambled))
